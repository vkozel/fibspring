1. Download apache-maven-3.5.2i (https://maven.apache.org/download.cgi)  and unpack it into your home directory.

2. Build project by running build/mavenBuild.sh

3. Run project with build/run.sh

4. Test example: http://localhost:8080/fibonacci?num=9
outputs: 0 1 1 2 3 5 8 13 21
