package fibspring;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class FibSpringController {
    


    @RequestMapping(value = "/fibonacci", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public String fibonacci(@RequestParam(value = "num", defaultValue = "1") String parm) {
            int num = Integer.valueOf(parm);
            return fibonacci(num);
    }



        String fibonacci(int n){
        if ( n <= 0) {
            //throw new IllegalArgumentException( "Wrong input: " + n);
            return new String("Please provide a positive number");
        }
        int[] arr = new int[n];
        if (n > 1) {
            arr[1] = 1;
        }
        for (int i = 2; i < n; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
             System.out.println("*************");
             System.out.println("Next int is "+arr[i]);
             System.out.println("*************");
         }
        StringBuffer ans = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            ans.append(Integer.toString(arr[i]));
            ans.append(" ");
        }
        
         
        return ans.toString();
    }
    
}
