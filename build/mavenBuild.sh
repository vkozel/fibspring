#!/bin/sh

#export M2_HOME=/Users/vkozel/Downloads/apache-maven-3.5.2
export M2_HOME=~/apache-maven-3.5.2
export PATH=$PATH:$M2_HOME/bin

cd $(dirname $0)

cd ../fibspring

rm -rf target

mvn clean package
ret=$?
if [ $ret -ne 0 ]; then
exit $ret
fi

exit
